Write a word wrapping line break using
impl Iterator<Item=&'a str>
as input and output, and
#![no_std]
(the problem is that you can't go back if you have to split a word from a previous iteration)
For instance "hello, how are you my friends" -> "hello, how are\n you my friends " if the line size is 16