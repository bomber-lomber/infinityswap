#![no_std]
#![no_main]

use core::str;
use infinityswap::libc_println;

#[panic_handler]
fn my_panic(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}

#[no_mangle]
pub extern "C" fn main(_argc: isize, _argv: *const *const u8) -> isize {
    let sample = "hello, how are you my friends!";
    let book = LimitedLineTextIterator::new(sample, 16);
    for line in book {
        libc_println!("{}", line);
    }
    0
}

struct LimitedLineTextIterator<'a> {
    raw_text: &'a str,
    line_size: usize,
    cursor_position: usize,
    cursor_position_byte: usize,
}

impl<'a> LimitedLineTextIterator<'a> {
    pub fn new(text: &'a str, line_size: usize) -> Self {
        Self {
            raw_text: text,
            line_size,
            cursor_position: 0,
            cursor_position_byte: 0,
        }
    }
}

impl<'a> Iterator for LimitedLineTextIterator<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        let byte_start = self.cursor_position_byte;
        let mut byte_end = self.cursor_position_byte;
        let mut chars = self.raw_text.chars().skip(self.cursor_position);
        let mut char_pos = 0;
        loop {
            if char_pos == self.line_size {
                break;
            }
            if let Some(c) = chars.next() {
                char_pos += 1;
                byte_end += c.len_utf8();
            } else {
                break;
            }
        }
        self.cursor_position += char_pos;
        self.cursor_position_byte = byte_end;

        if char_pos > 0 {
            Some(&self.raw_text[byte_start..byte_end])
        } else {
            None
        }
    }
}
